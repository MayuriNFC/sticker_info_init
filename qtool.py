import os
import json
import hashlib

def calculate_md5(file_path):
    md5 = hashlib.md5()
    with open(file_path, 'rb') as file:
        for chunk in iter(lambda: file.read(4096), b''):
            md5.update(chunk)
    return md5.hexdigest()

default_directory = '/storage/emulated/0/Documents/.fun/qq/本地表情包/'
directory = input("请输入文件夹路径：") or default_directory
print("当前路径为",directory)
json_data = {"paths": []}

try:
    for item in os.listdir(directory):
        item_path = os.path.join(directory, item)
        if os.path.isdir(item_path):
            info_file_path = os.path.join(item_path, "info.json")
            if os.path.exists("example.txt"):
                os.remove(info_file_path)
            json_data["paths"].append({"Name": item, "storePath": item})
            file_info_list = []
            for file_name in os.listdir(item_path):
                if os.path.isfile(os.path.join(item_path, file_name)):
                    file_info = {
                        "MD5": calculate_md5(os.path.join(item_path,file_name)),  # 在这里计算文件的MD5值或者留空
                        "fileName": file_name,
                        "addTime": os.path.getmtime(os.path.join(item_path, file_name))
                    }
                    file_info_list.append(file_info)
            with open(info_file_path, "w") as info_file:
                json.dump({"items": file_info_list}, info_file,ensure_ascii=False)
except:
    print("文件夹路径错误!Error!")
info_file_path = os.path.join(directory, "set.json")
with open(info_file_path, "w") as info_file:
    json.dump(json_data,info_file,ensure_ascii=False)
print("成功!Success!")
